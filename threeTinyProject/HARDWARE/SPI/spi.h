#ifndef __SPI_H
#define __SPI_H
#include "sys.h"
// SPI 总线速度设置
#define SPI_SPEED_2 0
#define SPI_SPEED_4 1
#define SPI_SPEED_8 2
#define SPI_SPEED_16 3
#define SPI_SPEED_32 4
#define SPI_SPEED_64 5
#define SPI_SPEED_128 6
#define SPI_SPEED_256 7
void SPI1_Init(void); //初始化 SPI1 口
void SPI1_SetSpeed(u8 SpeedSet); //设置 SPI1 速度
u8 SPI1_ReadWriteByte(u8 TxData); //SPI1 总线读写一个字节
#endif