#include "key.h"
#include "delay.h"
//按键初始化函数
void KEY_Init(void)
{
RCC->AHB1ENR|=1<<0; //使能 PORTA 时钟
RCC->AHB1ENR|=1<<4; //使能 PORTE 时钟
GPIO_Set(GPIOE,PIN3|PIN4,GPIO_MODE_IN,0,0,GPIO_PUPD_PU);
//PE3~4 设置上拉输入
GPIO_Set(GPIOA,PIN0,GPIO_MODE_IN,0,0,GPIO_PUPD_PD);
//PA0 设置为下拉输入
}
//按键处理函数
//返回按键值
//mode:0,不支持连续按;1,支持连续按;
//0，没有任何按键按下
//1，KEY0 按下
//2，KEY1 按下
//3，KEY_UP 按下 即 WK_UP
//注意此函数有响应优先级,KEY0>KEY1>KEY_UP!!
u8 KEY_Scan(u8 mode)
{
static u8 key_up=1; //按键按松开标志
if(mode)key_up=1; //支持连按
if(key_up&&(KEY0==0||KEY1==0||WK_UP==1))
{delay_ms(10);//去抖动
key_up=0;
if(KEY0==0)return 1;
else if(KEY1==0)return 2;
else if(WK_UP==1)return 3;
}else if(KEY0==1&&KEY1==1&&WK_UP==0)key_up=1;
return 0;// 无按键按下
}
