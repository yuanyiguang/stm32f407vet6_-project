#include "led.h"
//初始化 PA6 和 PA7 为输出口.并使能这两个口的时钟
//LED IO 初始化
void LED_Init(void)
{
RCC->AHB1ENR|=1<<0;//使能 PORTA 时钟
GPIO_Set(GPIOA,PIN6|PIN7,GPIO_MODE_OUT,GPIO_OTYPE_PP,
GPIO_SPEED_100M,GPIO_PUPD_PU); //PA6,PA7 设置
LED0=1;//LED0 关闭
LED1=1;//LED1 关闭
}
