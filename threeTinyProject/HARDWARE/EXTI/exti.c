#include "exti.h"
#include "delay.h"
#include "led.h"
#include "key.h"
//外部中断 0 服务程序
void EXTI0_IRQHandler(void)
{
delay_ms(10); //消抖
if(WK_UP==1) LED0=!LED0;
EXTI->PR=1<<0; //清除 LINE0 上的中断标志位
}
//外部中断 3 服务程序
void EXTI3_IRQHandler(void)
{
delay_ms(10); //消抖
if(KEY1==0) LED1=!LED1;
EXTI->PR=1<<3; //清除 LINE3 上的中断标志位
}
//外部中断 4 服务程序
void EXTI4_IRQHandler(void)
{
delay_ms(10); //消抖
if(KEY0==0)
{
LED0=!LED0;
LED1=!LED1;
}
EXTI->PR=1<<4; //清除 LINE4 上的中断标志位
}
//外部中断初始化程序
//初始化 PE2~4,PA0 为中断输入.
void EXTIX_Init(void)
{
KEY_Init();
Ex_NVIC_Config(GPIO_E,3,FTIR); //下降沿触发
Ex_NVIC_Config(GPIO_E,4,FTIR); //下降沿触发
Ex_NVIC_Config(GPIO_A,0,RTIR); //上升沿触发
MY_NVIC_Init(2,2,EXTI3_IRQn,2); //抢占 2，子优先级 2，组 2
MY_NVIC_Init(1,2,EXTI4_IRQn,2); //抢占 1，子优先级 2，组 2
MY_NVIC_Init(0,2,EXTI0_IRQn,2); //抢占 0，子优先级 2，组 2
}
