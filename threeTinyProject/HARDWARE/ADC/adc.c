#include "adc.h"
#include "delay.h"
//初始化 ADC
//这里我们仅以规则通道为例
//我们默认仅开启 ADC1_CH5
void Adc_Init(void)
{
//先初始化 IO 口
RCC->APB2ENR|=1<<8; //使能 ADC1 时钟
RCC->AHB1ENR|=1<<0; //使能 PORTA 时钟
GPIO_Set(GPIOA,PIN5,GPIO_MODE_AIN,0,0,GPIO_PUPD_PU); //PA5,模拟输入,下拉
RCC->APB2RSTR|=1<<8; //ADCs 复位
RCC->APB2RSTR&=~(1<<8); //复位结束
ADC->CCR=3<<16; //ADCCLK=PCLK2/4=84/4=21Mhz,ADC 时钟不要超过 36Mhz
ADC1->CR1=0; //CR1 设置清零
ADC1->CR2=0; //CR2 设置清零
ADC1->CR1|=0<<24; //12 位模式
ADC1->CR1|=0<<8; //非扫描模式
ADC1->CR2&=~(1<<1); //单次转换模式
ADC1->CR2&=~(1<<11); //右对齐
ADC1->CR2|=0<<28; //软件触发
ADC1->SQR1&=~(0XF<<20);
ADC1->SQR1|=0<<20; //1 个转换在规则序列中 也就是只转换规则序列 1
//设置通道 5 的采样时间
ADC1->SMPR2&=~(7<<(3*5)); //通道 5 采样时间清空
ADC1->SMPR2|=7<<(3*5); //通道 5 480 个周期,提高采样时间可以提高精确度
ADC1->CR2|=1<<0; //开启 AD 转换器
}
//获得 ADC 值
//ch:通道值 0~16
//返回值:转换结果
u16 Get_Adc(u8 ch)
{
	//设置转换序列
ADC1->SQR3&=0XFFFFFFE0;//规则序列 1 通道 ch
ADC1->SQR3|=ch;
ADC1->CR2|=1<<30; //启动规则转换通道
while(!(ADC1->SR&1<<1));//等待转换结束
return ADC1->DR; //返回 adc 值
}
//获取通道 ch 的转换值，取 times 次,然后平均
//ch:通道编号
//times:获取次数
//返回值:通道 ch 的 times 次转换结果平均值
u16 Get_Adc_Average(u8 ch,u8 times)
{
u32 temp_val=0;
u8 t;
for(t=0;t<times;t++)
{
temp_val+=Get_Adc(ch);
delay_ms(5);
}
return temp_val/times;
}
