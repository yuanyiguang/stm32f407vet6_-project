#include "lora_gateway.h"
#include "lora_ui.h"
#include "lora_cfg.h"
#include "usart3.h"
#include "string.h"
#include "led.h"
#include "delay.h"
#include "stdio.h"
#include "key.h"

//////////////////////////////////////////////////////////////////////////////////	   
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK STM32F407开发板
//ATK-LORA-02模块功能驱动	  
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//创建日期:2016/4/1
//版本：V1.0
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2014-2024
//All rights reserved
//******************************************************************************** 
//无

//设备参数初始化(具体设备参数见lora_cfg.h定义)
_LoRa_CFG LoRa_CFG=
{
	.addr = LORA_ADDR,       //设备地址
	.power = LORA_POWER,     //发射功率
	.chn = LORA_CHN,         //信道
	.wlrate = LORA_RATE,     //空中速率
	.wltime = LORA_WLTIME,   //睡眠时间
	.mode = LORA_MODE,       //工作模式
	.mode_sta = LORA_STA,    //发送状态
	.bps = LORA_TTLBPS ,     //波特率设置
	.parity = LORA_TTLPAR    //校验位设置
};

//设备工作模式(用于记录设备状态)
u8 Lora_mode=0;// 0:配置模式 1:接收模式 2:发送模式

//记录中断状态
static u8 Int_mode=0;//0：关闭 1:上升沿 2:下降沿

//AUX中断设置
//mode:配置的模式 0:关闭 1:上升沿 2:下降沿
void Aux_Int(u8 mode)
{
    if(!mode)
	{
        EXTI->IMR &=~(1<<6);//关闭
        NVIC->ICER[EXTI9_5_IRQn >> 0x05] =
        (uint32_t)0x01 << (EXTI9_5_IRQn & (uint8_t)0x1F);
	}else
	{
		if(mode==1)
		{
			EXTI->RTSR|=1<<6;	//line6上事件上升降沿触发
		}	
        else if(mode==2)
		{
			EXTI->FTSR|=1<<6;	//line6上事件下降沿触发
		}
	
		 EXTI->IMR |=(1<<6);//打开
		 NVIC->ISER[EXTI9_5_IRQn >> 0x05] =
        (uint32_t)0x01 << (EXTI9_5_IRQn & (uint8_t)0x1F);		
	}
	Int_mode = mode;//记录中断模式
 
}

//LORA_AUX中断服务函数
void EXTI9_5_IRQHandler(void)
{ 
    if(EXTI->PR & (1<<6))
	{  
       if(Int_mode==2)//下降沿(发送:开始发送数据 接收:数据开始输出)     
	   {
		  if(Lora_mode==1)//接收模式
		  {
			 USART3_RX_STA=0;//数据计数清0
		  }
		  Int_mode=1;//设置上升沿
		  LED0=!LED0;//DS0亮
	   }
       else if(Int_mode==1)//上升沿(发送:数据已发送完 接收:数据输出结束)	
	   {
		  if(Lora_mode==1)//接收模式
		  {
			 USART3_RX_STA|=1<<15;//数据计数标记完成
		  }else if(Lora_mode==2)//发送模式(串口数据发送完毕)
		  {
//			 Lora_mode=1;//进入接收模式
			  LED1=!LED1;//DS1灭	
		  }
		  Int_mode=2;//设置下降沿          	   
	   }
       Aux_Int(Int_mode);//重新设置中断边沿
	   EXTI->PR=1<<6;  //清除LINE4上的中断标志位  
	}
}

//LoRa模块初始化
//返回值:0,检测成功
//       1,检测失败
u8 LoRa_Init(void)
{
      u8 retry=0;
	  u8 temp=1;
	
	  RCC->AHB1ENR|=1<<2;     //使能PORTC时钟 
	  RCC->AHB1ENR|=1<<4;     //使能PORTF时钟 
	  GPIO_Set(GPIOC,PIN0|PIN1,GPIO_MODE_OUT,GPIO_OTYPE_PP,GPIO_SPEED_100M,GPIO_PUPD_PD); //PC0 PC1 下拉输出
	  GPIO_Set(GPIOE,PIN6,GPIO_MODE_IN,0,0,GPIO_PUPD_PU); 			//PE6设置为上拉输入 
//	  Ex_NVIC_Config(GPIO_E,6,RTIR); 		//上升沿触发
	  MY_NVIC_Init(0,2,EXTI9_5_IRQn,2);		//抢占3，子优先级2，组2 
	  EXTI->IMR &=~(1<<6);
      NVIC->ICER[EXTI9_5_IRQn >> 0x05] =
      (uint32_t)0x01 << (EXTI9_5_IRQn & (uint8_t)0x1F);
		 
	LORA_MD0=0;
	LORA_MD1=0;
	
	  while(!LORA_AUX)//确保LORA模块在空闲状态下(LORA_AUX=0)
	  {	
		delay_ms(100);		 
	  }
	  usart3_init(42,9600);//初始化串口3
	 
	  LORA_MD1=1;//进入配置模式
	  delay_ms(40);
	  retry=3;
	  while(retry--)
	  {
		 if(!lora_send_cmd("0xc1 0x00 0x04","OK",10))//等待100ms
		 {
			 temp=0;//检测成功
			 break;
		 }	
	  }
      if(retry==0) temp=1;//检测失败
	  return temp;  
}

//Lora模块参数配置
void LoRa_Set(void)
{
	
	while(!LORA_AUX);//等待模块空闲
	LORA_MD0=0;
	LORA_MD1=0; //进入配置模式
	delay_ms(40);
	
	USART3_RX_STA=0;
	Lora_mode=1;//标记"接收模式"
	Aux_Int(2);//设置LORA_AUX下降沿中断
	
}


u8 Tran_Data[30]={0};//透传数组


//Lora模块接收数据
void LoRa_ReceData(void)
{
    u16 len=0;
	u16 i;
   
	//有数据来了
	if(USART3_RX_STA&0x8000)
	{
		len = USART3_RX_STA&0X7FFF;
		USART3_RX_BUF[len]=0;//添加结束符
		USART3_RX_STA=0;

//		for(i=0;i<len;i++)
//		{		
//			while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
//		     USART1->DR=USART3_RX_BUF[i];  
//		}
		
		printf("%s",USART3_RX_BUF);
		memset((char*)USART3_RX_BUF,0x00,len);//串口接收缓冲区清0

	}

}

void LORA_FSM(u16 len)
{
	u8 i = 0;
	while(*USART3_RX_BUF != 0)
	{
		switch(USART3_RX_BUF[i])
		{
			case 1:
				
			break;
		}
	}
	
	
}

//发送和接收处理
void LoRa_Process(void)
{
	u8 key=0;
		
	LoRa_Set();//LoRa配置(进入配置需设置串口波特率为115200,) 
	while(1)
	{
		
//		key = KEY_Scan(0);
//		if(key==KEY1_PRES)//发送数据
//		{
////			break;
////			else
//			  if(1)//空闲且非省电模式
//			  {
//				  Lora_mode=2;//标记"发送状态"
//				  LoRa_SendData();//发送数据    
//			  }
////			  delay_ms(100);
//		}
			
		//数据接收
		LoRa_ReceData();		
   }
	
}

//主测试函数
void Lora_Test(void)
{	
	u8 t=0;
	u8 key=0;
	while(LoRa_Init())//初始化ATK-LORA-02模块
	{
		printf("未检测到模块!!!"); 	
		delay_ms(300);
	}
	printf("检测到模块!!!");
    delay_ms(500); 		
	while(1)
	{		
		key = KEY_Scan(0);
		if(key)
		{		
			if(key==WKUP_PRES)//KEY_UP按下
			{
				  LoRa_Process();//开始数据测试				
			}
		}		
		t++;
		if(t==30)
		{
			t=0;
			LED0=~LED0;
		}
		delay_ms(10);
	}	
	
}













